CC="gcc"
INCLUDES= -lX11 -I/usr/X11R6/include -I/usr/X11R6/include/X11 -L/usr/X11R6/lib -L/usr/X11R6/lib/X11

build:
	${CC} main.c -o chip8 ${INCLUDES}

run: build
	./chip8

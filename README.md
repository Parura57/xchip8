# Chip-8 emulator

xchip8, a chip emulator written in C with XLib, with some potentially broken parts.

Because it uses XResizeWindow to keep the window size fixed, the emulator display may be broken on some tiling window managers

Since it uses XLib, it will work only on systems running an X session, ie. most Linux distributions and Mac systems

**WARNING: After the program is closed improperly, you might find your key autorepeat broken. To fix it, rerun the program and close it using ESC in the emulator window or SIGINT**

## Running

To compile:

    $ make

To compile and run a test suite from [here](https://github.com/corax89/chip8-test-rom):

    $ make run

And to run a ROM:

    $ make
    $ ./chip8 /path/to/file


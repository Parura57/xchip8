#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

// TODO failsafes on JP, CALL, LD I, JP V0 against memory leaks


void copyBits(char input, char *output) {       // Give *output as output+offset
    output[0] = input & 0b10000000;
    output[1] = input & 0b01000000;
    output[2] = input & 0b00100000;
    output[3] = input & 0b00010000;
    output[4] = input & 0b00001000;
    output[5] = input & 0b00000100;
    output[6] = input & 0b00000010;
    output[7] = input & 0b00000001;
}

unsigned char cont = 1;

int main (int argc, char *argv[]) {
    if (argc != 2) {printf("Usage: chip8 [ROM]\n");; return -1;}
    // Initialize window
    Display *dpy = XOpenDisplay(getenv("DISPLAY"));
    int s = DefaultScreen(dpy);
    int xwidth = 640;
    int ywidth = 320;
    Window win = XCreateSimpleWindow(dpy, RootWindow(dpy, s), 10, 10, xwidth, ywidth, 1, BlackPixel(dpy, s), WhitePixel(dpy,s));
    XStoreName(dpy, win, "XSnek");
    XSelectInput(dpy, win, ButtonPressMask|KeyPressMask|StructureNotifyMask|KeyReleaseMask|KeymapStateMask);
    XAutoRepeatOff(dpy);
    XMapWindow(dpy, win);
    XFlush(dpy);

    // Initialize context
    XGCValues values;
    GC gc = XCreateGC(dpy, win, 0, &values);
    XSetForeground(dpy, gc, BlackPixel(dpy, s));
    //XSetBackground(dpy, gc, BlackPixel(dpy, s));
    XSetLineAttributes(dpy, gc, 2, LineSolid, CapButt, JoinBevel);
    XSetFillStyle(dpy, gc, FillSolid);
    
    // Variables declaration
    printf("Allocating memory...\n");
    unsigned char memory[4096];
    //for (int i=0;i<4200;i++) {printf("%i i%i; ", memory[i], i); memory[i] = 0x00;}
    for (int i=0;i<4200;i++) memory[i] = 0x00;
    printf("Allocating registers...\n");
    char V[16];
    //for (int i=0;i<16;i++) {printf("%i i%i; ", V[i], i); V[i] = 0x00;}
    for (int i=0;i<16;i++) V[i] = 0x00;
    unsigned char DT = 0x00;
    unsigned char ST = 0x00;
    unsigned short I = 0x0000;
    unsigned short PC = 0x0200;      // Program starts at 0x200, earlier memory is allocated for the emulator
    unsigned short SP = 0x0000;
    unsigned short stack[65536];
    printf("Allocating stack...\n");
    //for (int i=0;i<65536;i++) {printf("%i i%i ", stack[i], i); stack[i] = 0x0;}
    for (int i=0;i<65536;i++) stack[i] = 0x0;
    unsigned char display[32][64];
    for (int i=0;i<64;i++) {
        for (int j=0;i<32;i++) display[j][i] = 0x0;
    }

    // Load ROM into memory
    FILE *fd;
    fd = fopen(argv[1], "r");
    char buf = fgetc(fd);
    int i = 0x200;
    while(!feof(fd)) {
        memory[i] = buf;
        printf("loading %x at %i\n", memory[i], i);
        i++;
        if (i > 4096) return 1;
        buf = fgetc(fd);
    }
    fclose(fd);

    // Load fonts
    memory[0] = 0b11110000; // 0
    memory[1] = 0b10010000;
    memory[2] = 0b10010000;
    memory[3] = 0b10010000;
    memory[4] = 0b11110000;

    memory[5] = 0b00100000; // 1
    memory[6] = 0b01100000;
    memory[7] = 0b00100000;
    memory[8] = 0b00100000;
    memory[9] = 0b01110000;

    memory[10] =0b11110000; // 2
    memory[11] =0b00010000;
    memory[12] =0b11110000;
    memory[13] =0b10000000;
    memory[14] =0b11110000;

    memory[15] =0b11110000; // 3
    memory[16] =0b00010000;
    memory[17] =0b01110000;
    memory[18] =0b00010000;
    memory[19] =0b11110000;

    memory[20] =0b10010000; // 4
    memory[21] =0b10010000;
    memory[22] =0b11110000;
    memory[23] =0b00010000;
    memory[24] =0b00010000;

    memory[25] =0b11110000; // 5
    memory[26] =0b10000000;
    memory[27] =0b11110000;
    memory[28] =0b00010000;
    memory[29] =0b11110000;

    memory[30] =0b11110000; // 6
    memory[31] =0b10000000;
    memory[32] =0b11110000;
    memory[33] =0b10010000;
    memory[34] =0b11110000;

    memory[35] =0b11110000; // 7
    memory[36] =0b00010000;
    memory[37] =0b00010000;
    memory[38] =0b00010000;
    memory[39] =0b00010000;

    memory[40] =0b11110000; // 8
    memory[41] =0b10010000;
    memory[42] =0b11110000;
    memory[43] =0b10010000;
    memory[44] =0b11110000;

    memory[45] =0b11110000; // 9
    memory[46] =0b10010000;
    memory[47] =0b11110000;
    memory[48] =0b00010000;
    memory[49] =0b11110000;

    memory[50] =0b11110000; // A
    memory[51] =0b10010000;
    memory[52] =0b11110000;
    memory[53] =0b10010000;
    memory[54] =0b10010000;

    memory[55] =0b11100000; // B
    memory[56] =0b10010000;
    memory[57] =0b11100000;
    memory[58] =0b10010000;
    memory[59] =0b11100000;

    memory[60] =0b11110000; // C
    memory[61] =0b10000000;
    memory[62] =0b10000000;
    memory[63] =0b10000000;
    memory[64] =0b11110000;

    memory[65] =0b11100000; // D
    memory[66] =0b10010000;
    memory[67] =0b10010000;
    memory[68] =0b10010000;
    memory[69] =0b11100000;

    memory[70] =0b11110000; // E
    memory[71] =0b10000000;
    memory[72] =0b11110000;
    memory[73] =0b10000000;
    memory[74] =0b11110000;

    memory[75] =0b11110000; // F
    memory[76] =0b10000000;
    memory[77] =0b11110000;
    memory[78] =0b10000000;
    memory[79] =0b10000000;


    // Dirty way to wait for display to initialise and clear it
    usleep(1*1000000);  
    printf("Ready\n");
    XSetForeground(dpy, gc, BlackPixel(dpy, s));
    XFillRectangle(dpy, win, gc, 0, 0, xwidth, ywidth);
    XFlush(dpy);

    // Main loop
    XEvent event;
    unsigned short instruction;
    char currentkey = 0x0;
    void stopmain() {cont = 0;}
    signal(2, stopmain);        // Intercept SIGINT
    unsigned short keyspressed = 0b0000000000000000;
    unsigned short keymask[16] = {0b0000000000000001,
                                  0b0000000000000010,
                                  0b0000000000000100,
                                  0b0000000000001000,
                                  0b0000000000010000,
                                  0b0000000000100000,
                                  0b0000000001000000,
                                  0b0000000010000000,
                                  0b0000000100000000,
                                  0b0000001000000000,
                                  0b0000010000000000,
                                  0b0000100000000000,
                                  0b0001000000000000,
                                  0b0010000000000000,
                                  0b0100000000000000,
                                  0b1000000000000000};
    while(cont) {
        while (XPending(dpy) > 0) {
            XNextEvent(dpy, &event);
            if (event.type == ClientMessage) cont = 0;
            else if (event.type == KeymapNotify) XRefreshKeyboardMapping(&event.xmapping);
            else if (event.type == KeyPress) {
                //printf("keycode %i\n", event.xkey.keycode);
                switch (event.xkey.keycode) {
                case 9: cont = 0;                                           // ESC-close program
                case 10: keyspressed = keyspressed | keymask[0x1]; break;    // 1-1
                case 11: keyspressed = keyspressed | keymask[0x2]; break;    // 2-2
                case 12: keyspressed = keyspressed | keymask[0x3]; break;    // 3-3
                case 13: keyspressed = keyspressed | keymask[0xC]; break;    // 4-C

                case 24: keyspressed = keyspressed | keymask[0x4]; break;    // Q-4
                case 25: keyspressed = keyspressed | keymask[0x5]; break;    // W-5
                case 26: keyspressed = keyspressed | keymask[0x6]; break;    // E-6
                case 27: keyspressed = keyspressed | keymask[0xD]; break;    // R-D

                case 38: keyspressed = keyspressed | keymask[0x7]; break;    // A-7
                case 39: keyspressed = keyspressed | keymask[0x8]; break;    // S-8
                case 40: keyspressed = keyspressed | keymask[0x9]; break;    // D-9
                case 41: keyspressed = keyspressed | keymask[0xE]; break;    // F-E

                case 52: keyspressed = keyspressed | keymask[0xA]; break;    // Z-A
                case 53: keyspressed = keyspressed | keymask[0x0]; break;    // X-0
                case 54: keyspressed = keyspressed | keymask[0xB]; break;    // C-B
                case 55: keyspressed = keyspressed | keymask[0xF]; break;    // V-F
                }
            }
            if (event.type == KeyRelease) {
                //printf("keycoder %i\n", event.xkey.keycode);
                switch (event.xkey.keycode) {   // Will break if a key is released somehow without being pressed
                case 10: keyspressed = keyspressed ^ keymask[0x1]; break;    // 1-1
                case 11: keyspressed = keyspressed ^ keymask[0x2]; break;    // 2-2
                case 12: keyspressed = keyspressed ^ keymask[0x3]; break;    // 3-3
                case 13: keyspressed = keyspressed ^ keymask[0xC]; break;    // 4-C

                case 24: keyspressed = keyspressed ^ keymask[0x4]; break;    // Q-4
                case 25: keyspressed = keyspressed ^ keymask[0x5]; break;    // W-5
                case 26: keyspressed = keyspressed ^ keymask[0x6]; break;    // E-6
                case 27: keyspressed = keyspressed ^ keymask[0xD]; break;    // R-D

                case 38: keyspressed = keyspressed ^ keymask[0x7]; break;    // A-7
                case 39: keyspressed = keyspressed ^ keymask[0x8]; break;    // S-8
                case 40: keyspressed = keyspressed ^ keymask[0x9]; break;    // D-9
                case 41: keyspressed = keyspressed ^ keymask[0xE]; break;    // F-E

                case 52: keyspressed = keyspressed ^ keymask[0xA]; break;    // Z-A
                case 53: keyspressed = keyspressed ^ keymask[0x0]; break;    // X-0
                case 54: keyspressed = keyspressed ^ keymask[0xB]; break;    // C-B
                case 55: keyspressed = keyspressed ^ keymask[0xF]; break;    // V-F
                }
            }
        }
        XResizeWindow(dpy, win, xwidth, ywidth);

        // Switch instructions
        instruction = memory[PC]*256 + memory[PC+1];
        printf("\n0x%x %x keys %x I:%x *%x ", PC, instruction, keyspressed, I, memory[I]);
        for (int i = 0; i < 16; i++) printf("V%x:%x ", i, V[i]);
        switch (instruction & 0xF000) {
        case 0x0000:
            if (instruction == 0x00E0) {                // CLS [00E0]
                for (int i=0;i<64;i++) {
                    for (int j=0;i<32;i++) display[j][i] = 0x0;
                }
                printf("CLS");
           } else if (instruction == 0x00EE) {         // RET [00EE]
                PC = stack[SP];
                SP--;
                printf("RET");
            }
            break;


        case 0x1000:                                      // JP addr [1nnn]
            PC = (instruction & 0x0FFF) - 2;
            printf("JP");
            break;


        case 0x2000:                                      // CALL addr [2nnn]
            SP++;
            stack[SP] = PC;
            PC = (instruction & 0x0FFF) - 2;
            printf("CALL");
            break;


        case 0x3000:                                      // SE Vx,byte [3xkk]
            if ((unsigned char) V[(instruction & 0x0F00) / 0x0100] == (unsigned char) instruction & 0x00FF) PC += 2;
            printf("SE %x %x", V[(instruction & 0x0F00) / 0x0100], instruction & 0x00FF);
            break;


        case 0x4000:                                      // SNE Vx,byte [4xkk]
            if ((unsigned char) V[(instruction & 0x0F00) / 0x0100] != (unsigned char) instruction & 0x00FF) PC += 2;
            printf("SNE");
            break;


        case 0x5000:                                      // SE Vx,Vy [5xy0]
            if ((unsigned char) V[(instruction & 0x0F00) / 0x0100] == (unsigned char) V[(instruction & 0x00F0) / 0x0010] && instruction & 0x000F == 0x0000) PC += 2;
            printf("SE");
            break;


        case 0x6000:                                      // LD Vx,byte [6xkk]
            V[(instruction & 0x0F00) / 0x0100] = instruction & 0x00FF;
            printf("LD");
            break;


        case 0x7000:                                      // ADD Vx,byte [7xkk]
            V[(instruction & 0x0F00) / 0x0100] += instruction & 0x00FF;
            printf("ADD");
            break;


        case 0x8000:            
            switch (instruction & 0x000F) {
            case 0x0000:                                   // LD Vx,Vy [8xy0]
                V[(instruction & 0x0F00) / 0x0100] = V[(instruction & 0x00F0) / 0x0010];
                printf("LD");
                break;

            case 0x0001:                                   // OR Vx,Vy [8xy1]
                V[(instruction & 0x0F00) / 0x0100] = V[(instruction & 0x00F0) / 0x0010] | V[(instruction & 0x0F00) / 0x0100];
                printf("OR");
                break;

            case 0x0002:                                   // AND Vx,Vy [8xy2]
                V[(instruction & 0x0F00) / 0x0100] = V[(instruction & 0x00F0) / 0x0010] & V[(instruction & 0x0F00) / 0x0100];
                printf("AND");
                break;

            case 0x0003:                                   // XOR Vx,Vy [8xy3]
                V[(instruction & 0x0F00) / 0x0100] = V[(instruction & 0x00F0) / 0x0010] ^ V[(instruction & 0x0F00) / 0x0100];
                printf("XOR");
                break;

            case 0x0004:                                   // ADD Vx,Vy [8xy4]
                V[(instruction & 0x0F00) / 0x0100] += V[(instruction & 0x00F0) / 0x0010];
                printf("ADD");
                break;

            case 0x0005:                                   // SUB Vx,Vy [8xy5]
                V[(instruction & 0x0F00) / 0x0100] -= V[(instruction & 0x00F0) / 0x0010];
                printf("SUB");
                break;

            case 0x0006:                                   // SHR Vx{,Vy} [8xy6]
                V[(instruction & 0b00000001) / 0b00000001] = V[0xF];
                V[(instruction & 0x0F00) / 0x0100] = V[(instruction & 0x0F00) / 0x0100] / 2;
                printf("SHR");
                break;

            case 0x0007:                                   // SUBN Vx,Vy [8xy7]
                V[0xF] = V[(instruction & 0x00F0) / 0x0010] > V[(instruction & 0x0F00) / 0x0100];
                V[(instruction & 0x0F00) / 0x0100] = V[(instruction & 0x00F0) / 0x0010] - V[(instruction & 0x0F00) / 0x0100];
                printf("SUBN");
                break;

            case 0x000E:                                   // SHL Vx{,Vy} [8xyE]
                V[(instruction & 0b10000000) / 0b10000000] = V[0xF];
                V[(instruction & 0x0F00) / 0x0100] = V[(instruction & 0x0F00) / 0x0100] * 2;
                printf("SHL");
                break;

            default:
                printf("Invalid instruction %i (%x)\n", instruction, instruction);
                break;
            }
            break;


        case 0x9000:                                       // SNE Vx,Vy [9xy0]
            if (V[(instruction & 0x0F00) / 0x0100] != V[(instruction & 0x00F0) / 0x0010] && instruction & 0x000F == 0x0000) PC += 2;
            printf("SNE");
            break;


        case 0xA000:                                       // LD I,addr [Annn]
            I = instruction & 0x0FFF;
            printf("LDI");
            break;


        case 0xB000:                                       // JP V0,addr [Bnnn]
            PC = V[0] + (instruction & 0x0FFF) - 2;
            printf("JP");
            break;


        case 0xC000:                                       // RND Vx,byte [Cxkk]
            srand(instruction & 0x00FF);
            V[(instruction & 0xF000) / 0x1000] = rand();
            printf("RND");
            break;


        case 0xD000:                                       // DRW Vx,Vy,n [Dxyn]
            V[0xF] = 0x0; 
            int x = V[(instruction & 0x0F00) / 0x0100];
            int y = V[(instruction & 0x00F0) / 0x0010];
            for (int i = I; i < (instruction & 0x000F) + I; i++) {
                //printf("%x %x\n", x, y);
                if (display[y][x] == 0x0) copyBits(memory[i], display[y]+x);
                else {if (memory[i] = 0x0) V[0xF] = 0x1; copyBits(memory[i], display[y]+x);}
                y++;
                if(x > 63) x = 0;   // Does wrapping mean y also goes up?
            }


            /*for (int i = 0; i < 32; i++) {
                for (int j = 0; j < 64; j++) {
                    printf("%x", display[i][j]);
                }
                printf("\n");
            }*/

            printf("DRW");
            break;


        case 0xE000:
            if ((unsigned char) (instruction & 0x00FF) == (unsigned char) 0x009E) {           // SKP Vx [Ex9E]
                if (keyspressed & (keymask[(instruction & 0x0F00) / 0x0100])) PC += 2;
                printf("SKP\n0x%x \n0x%x \n", keyspressed, (instruction & 0x0F00) / 0x0100);
            }

            else if ((unsigned char) (instruction & 0x00FF) == (unsigned char) 0x00A1) {      // SKNP Vx [ExA1]
                if (!(keyspressed & (keymask[instruction & 0x0F00 / 0x0100]))) PC += 2;
                printf("SKNP\n0x%x \n0x%x \n", keyspressed, (instruction & 0x0F00) / 0x0100);
            }
            else printf("Invalid 0xE instruction %x %x %x\n", instruction, (unsigned char) (instruction & 0x00FF), (unsigned char) 0x00A1);
            break;


        case 0xF000:
            switch (instruction & 0x00FF) {
            case 0x0007:                                    // LD Vx,DT [Fx07]
                V[(instruction & 0x0F00) / 0x0100] = DT;
                printf("LD");
                break;

            case 0x000A:                                    // LD Vx,K [Fx0A]
                ldkey:
                XNextEvent(dpy, &event);
                if (event.type == KeyPress) switch (event.xkey.keycode) {
                    case 10: V[(instruction & 0x0F00) / 0x0100] = 0x1; break;    // 1-1
                    case 11: V[(instruction & 0x0F00) / 0x0100] = 0x2; break;    // 2-2
                    case 12: V[(instruction & 0x0F00) / 0x0100] = 0x3; break;    // 3-3
                    case 13: V[(instruction & 0x0F00) / 0x0100] = 0xC; break;    // 4-C

                    case 24: V[(instruction & 0x0F00) / 0x0100] = 0x4; break;    // Q-4
                    case 25: V[(instruction & 0x0F00) / 0x0100] = 0x5; break;    // W-5
                    case 26: V[(instruction & 0x0F00) / 0x0100] = 0x6; break;    // E-6
                    case 27: V[(instruction & 0x0F00) / 0x0100] = 0xD; break;    // R-D

                    case 38: V[(instruction & 0x0F00) / 0x0100] = 0x7; break;    // A-7
                    case 39: V[(instruction & 0x0F00) / 0x0100] = 0x8; break;    // S-8
                    case 40: V[(instruction & 0x0F00) / 0x0100] = 0x9; break;    // D-9
                    case 41: V[(instruction & 0x0F00) / 0x0100] = 0xE; break;    // F-E

                    case 52: V[(instruction & 0x0F00) / 0x0100] = 0xA; break;    // Z-A
                    case 53: V[(instruction & 0x0F00) / 0x0100] = 0x0; break;    // X-0
                    case 54: V[(instruction & 0x0F00) / 0x0100] = 0xB; break;    // C-B
                    case 55: V[(instruction & 0x0F00) / 0x0100] = 0xF; break;    // V-F
                }
                else goto ldkey;
                printf("LD");
                break;

            case 0x0015:                                // LD DT,Vx [Fx15]
                DT = V[(instruction & 0x0F00) / 0x0100];
                printf("LD");
                break;

            case 0x0018:                                // LD ST,Vx [Fx18]
                ST = V[(instruction & 0x0F00) / 0x0100];
                printf("LD");
                break;

            case 0x001E:                                // ADD I,Vx [Fx1E]
                I += V[(instruction & 0x0F00) / 0x0100];
                printf("ADD");
                break;

            case 0x0029:                                // LD F,Vx [Fx29]
                I = V[(instruction & 0x0F00) / 0x0100] * 5;
                printf("LD");
                break;

            case 0x0033:                                // LD B,Vx [Fx33]
                if(I > 4093) {printf("INVALID MEMORY ADDRESS\n"); return 1;}
                memory[I] = (V[(instruction & 0x0F00) / 0x0100] & 0xFF) / 100;
                memory[I+1] = ((V[(instruction & 0x0F00) / 0x0100] & 0xFF) - memory[I]*100) / 10;
                memory[I+2] = (V[(instruction & 0x0F00) / 0x0100] & 0xFF) - memory[I+1]*10 - memory[I]*100;
                printf("\n%x %x %x\n", memory[I], memory[I+1], memory[I+2]);
                printf("LD");
                break;

            case 0x0055:                                // LD [I],Vx [Fx55]
                for (int i = 0; i <= (instruction & 0x0F00) / 0x0100; i++) memory[I+i] = V[i];
                printf("LD");
                break;
 
            case 0x0065:                                // LD Vx,[I] [Fx65]
                for (int i = 0; i <= (instruction & 0x0F00) / 0x0100; i++) V[i] = memory[I+i];
                printf("LD");
                break;

            default:
                printf("Invalid instruction %x\n", instruction);
            }
            break;


        default:
            printf("Invalid instruction %i (%x)\n", instruction, instruction);
            break;
        }
        PC += 2;
        if (PC > 4095)

        // Decrease counters
        if(DT > 0) DT--;
        if(ST > 0) {
            ST--;
            if(ST == 0) XBell(dpy, 75);      // TODO test
        }

        // Render
        XSetForeground(dpy, gc, BlackPixel(dpy, s));
        XFillRectangle(dpy, win, gc, 0, 0, xwidth, ywidth);
        XSetForeground(dpy, gc, WhitePixel(dpy, s));
        for (int i = 0; i < 64; i++) {
            for (int j = 0; j < 32; j++) {
                if (display[j][i]) XFillRectangle(dpy, win, gc, i*10, j*10, 10, 10);
                //printf("%x", display[j][i]);
            }
            //printf("\n");
        }
        XFlush(dpy);

        usleep(0.1*1000000);
    }

    // Exit
    XFlush(dpy);
    XDestroyWindow(dpy, win);
    XAutoRepeatOn(dpy);
    XCloseDisplay(dpy);
    printf("\nExited properly\n");
    return 0;
}

